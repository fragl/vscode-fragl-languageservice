/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import { createScanner } from './parser/fraglScanner';
import { parse } from './parser/fraglParser';
import { FRAGLCompletion } from './services/fraglCompletion';
import { FRAGLHover } from './services/fraglHover';
import { format } from './services/fraglFormatter';
import { findDocumentLinks } from './services/fraglLinks';
import { findDocumentHighlights } from './services/fraglHighlighting';
import { findDocumentSymbols } from './services/fraglSymbolsProvider';
import { doRename } from './services/fraglRename';
import { findMatchingTagPosition } from './services/fraglMatchingTagPosition';
import { findLinkedEditingRanges } from './services/fraglLinkedEditing';
import {
	Scanner, FRAGLDocument, CompletionConfiguration, ICompletionParticipant, FRAGLFormatConfiguration, DocumentContext,
	IFRAGLDataProvider, FRAGLDataV1, LanguageServiceOptions, TextDocument, SelectionRange, WorkspaceEdit,
	Position, CompletionList, Hover, Range, SymbolInformation, TextEdit, DocumentHighlight, DocumentLink, FoldingRange, HoverSettings
} from './fraglLanguageTypes';
import { getFoldingRanges } from './services/fraglFolding';
import { getSelectionRanges } from './services/fraglSelectionRange';
import { FRAGLDataProvider } from './languageFacts/dataProvider';
import { FRAGLDataManager } from './languageFacts/dataManager';
import { fraglData } from './languageFacts/data/webCustomData';

export * from './fraglLanguageTypes';

export interface LanguageService {
	setDataProviders(useDefaultDataProvider: boolean, customDataProviders: IFRAGLDataProvider[]): void;
	createScanner(input: string, initialOffset?: number): Scanner;
	parseFRAGLDocument(document: TextDocument): FRAGLDocument;
	findDocumentHighlights(document: TextDocument, position: Position, fraglDocument: FRAGLDocument): DocumentHighlight[];
	doComplete(document: TextDocument, position: Position, fraglDocument: FRAGLDocument, options?: CompletionConfiguration): CompletionList;
	doComplete2(document: TextDocument, position: Position, fraglDocument: FRAGLDocument, documentContext: DocumentContext, options?: CompletionConfiguration): Promise<CompletionList>;
	setCompletionParticipants(registeredCompletionParticipants: ICompletionParticipant[]): void;
	doHover(document: TextDocument, position: Position, fraglDocument: FRAGLDocument, options?: HoverSettings): Hover | null;
	format(document: TextDocument, range: Range | undefined, options: FRAGLFormatConfiguration): TextEdit[];
	findDocumentLinks(document: TextDocument, documentContext: DocumentContext): DocumentLink[];
	findDocumentSymbols(document: TextDocument, fraglDocument: FRAGLDocument): SymbolInformation[];
	doTagComplete(document: TextDocument, position: Position, fraglDocument: FRAGLDocument): string | null;
	getFoldingRanges(document: TextDocument, context?: { rangeLimit?: number }): FoldingRange[];
	getSelectionRanges(document: TextDocument, positions: Position[]): SelectionRange[];
	doRename(document: TextDocument, position: Position, newName: string, fraglDocument: FRAGLDocument): WorkspaceEdit | null;
	findMatchingTagPosition(document: TextDocument, position: Position, fraglDocument: FRAGLDocument): Position | null;
	/** Deprecated, Use findLinkedEditingRanges instead */
	findOnTypeRenameRanges(document: TextDocument, position: Position, fraglDocument: FRAGLDocument): Range[] | null;
	findLinkedEditingRanges(document: TextDocument, position: Position, fraglDocument: FRAGLDocument): Range[] | null;
}

const defaultLanguageServiceOptions = {};

export function getLanguageService(options: LanguageServiceOptions = defaultLanguageServiceOptions): LanguageService {
	const dataManager = new FRAGLDataManager(options);

	const fraglHover = new FRAGLHover(options, dataManager);
	const fraglCompletion = new FRAGLCompletion(options, dataManager);

	return {
		setDataProviders: dataManager.setDataProviders.bind(dataManager),
		createScanner,
		parseFRAGLDocument: document => parse(document.getText()),
		doComplete: fraglCompletion.doComplete.bind(fraglCompletion),
		doComplete2: fraglCompletion.doComplete2.bind(fraglCompletion),
		setCompletionParticipants: fraglCompletion.setCompletionParticipants.bind(fraglCompletion),
		doHover: fraglHover.doHover.bind(fraglHover),
		format,
		findDocumentHighlights,
		findDocumentLinks,
		findDocumentSymbols,
		getFoldingRanges,
		getSelectionRanges,
		doTagComplete: fraglCompletion.doTagComplete.bind(fraglCompletion),
		doRename,
		findMatchingTagPosition,
		findOnTypeRenameRanges: findLinkedEditingRanges,
		findLinkedEditingRanges
	};
}

export function newFRAGLDataProvider(id: string, customData: FRAGLDataV1): IFRAGLDataProvider {
	return new FRAGLDataProvider(id, customData);
}

export function getDefaultFRAGLDataProvider(): IFRAGLDataProvider {
	return newFRAGLDataProvider('default', fraglData);
}

