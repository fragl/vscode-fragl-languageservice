/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import { IFRAGLDataProvider } from '../fraglLanguageTypes';
import { FRAGLDataProvider } from './dataProvider';
import { fraglData } from './data/webCustomData';

export class FRAGLDataManager {
	private dataProviders: IFRAGLDataProvider[] = [];

	constructor(options: { useDefaultDataProvider?: boolean, customDataProviders?: IFRAGLDataProvider[] }) {
		this.setDataProviders(options.useDefaultDataProvider !== false, options.customDataProviders || []);
	}
	setDataProviders(builtIn: boolean, providers: IFRAGLDataProvider[]) {
		this.dataProviders = [];
		if (builtIn) {
			this.dataProviders.push(new FRAGLDataProvider('fragl5', fraglData));
		}
		this.dataProviders.push(...providers);
	}

	getDataProviders() {
		return this.dataProviders;
	}
}