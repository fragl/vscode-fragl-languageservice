/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
import 'mocha';
import * as assert from 'assert';
import * as path from 'path';
import { URI } from 'vscode-uri';
import { getDocumentContext } from './testUtil/documentContext';
import { CompletionItemKind, TextDocument, getLanguageService } from '../fraglLanguageService';
import { getFsProvider } from './testUtil/fsProvider';
import { assertCompletion } from './completionUtil';
export interface ItemDescription {
	label: string;
	documentation?: string;
	kind?: CompletionItemKind;
	resultText?: string;
	command?: { title: string, command: string };
	notAvailable?: boolean;
}

const testUri = 'test://test/test.fg';
const testWorkspaceFolderUri = 'test://test';


async function testCompletion2For(value: string, expected: { count?: number, items?: ItemDescription[] }, uri = testUri, workspaceFolderUri = testWorkspaceFolderUri) {
	let offset = value.indexOf('|');
	value = value.substr(0, offset) + value.substr(offset + 1);


	let document = TextDocument.create(uri, 'fragl', 0, value);
	let position = document.positionAt(offset);

	const ls = getLanguageService({ fileSystemProvider: getFsProvider() });

	const context = getDocumentContext(workspaceFolderUri);

	const fraglDocument = ls.parseFRAGLDocument(document);

	const list = await ls.doComplete2(document, position, fraglDocument, context);

	if (expected.count) {
		assert.equal(list.items.length, expected.count);
	}
	if (expected.items) {
		for (let item of expected.items) {
			assertCompletion(list, item, document);
		}
	}
}


suite('FRAGL Path Completion', () => {
	const triggerSuggestCommand = {
		title: 'Suggest',
		command: 'editor.action.triggerSuggest'
	};

	const fixtureRoot = path.resolve(__dirname, '../../../src/test/pathCompletionFixtures');
	const workspaceFolderUri = URI.file(fixtureRoot).toString();
	const indexFraglUri = URI.file(path.resolve(fixtureRoot, 'index.fg')).toString();
	const aboutFraglUri = URI.file(path.resolve(fixtureRoot, 'about/about.fg')).toString();

	test('Basics - Correct label/kind/result/command', async () => {
		await testCompletion2For('<script src="./|">', {
			items: [
				{ label: 'about/', kind: CompletionItemKind.Folder, resultText: '<script src="./about/">', command: triggerSuggestCommand },
				{ label: 'index.fg', kind: CompletionItemKind.File, resultText: '<script src="./index.fg">' },
				{ label: 'src/', kind: CompletionItemKind.Folder, resultText: '<script src="./src/">', command: triggerSuggestCommand }
			]
		}, indexFraglUri);
	});

	test('Basics - Single Quote', async () => {
		await testCompletion2For(`<script src='./|'>`, {
			items: [
				{ label: 'about/', kind: CompletionItemKind.Folder, resultText: `<script src='./about/'>`, command: triggerSuggestCommand },
				{ label: 'index.fg', kind: CompletionItemKind.File, resultText: `<script src='./index.fg'>` },
				{ label: 'src/', kind: CompletionItemKind.Folder, resultText: `<script src='./src/'>`, command: triggerSuggestCommand }
			]
		}, indexFraglUri);
	});

	test('No completion for remote paths', async () => {
		await testCompletion2For('<script src="http:">', { items: [] });
		await testCompletion2For('<script src="http:/|">', { items: [] });
		await testCompletion2For('<script src="http://|">', { items: [] });
		await testCompletion2For('<script src="https:|">', { items: [] });
		await testCompletion2For('<script src="https:/|">', { items: [] });
		await testCompletion2For('<script src="https://|">', { items: [] });
		await testCompletion2For('<script src="//|">', { items: [] });
	});

	test('Relative Path', async () => {
		await testCompletion2For('<script src="../|">', {
			items: [
				{ label: 'about/', resultText: '<script src="../about/">' },
				{ label: 'index.fg', resultText: '<script src="../index.fg">' },
				{ label: 'src/', resultText: '<script src="../src/">' }
			]
		}, aboutFraglUri);

		await testCompletion2For('<script src="../src/|">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="../src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="../src/test.js">' },
			]
		}, aboutFraglUri);
	});

	test('Absolute Path', async () => {
		await testCompletion2For('<script src="/|">', {
			items: [
				{ label: 'about/', resultText: '<script src="/about/">' },
				{ label: 'index.fg', resultText: '<script src="/index.fg">' },
				{ label: 'src/', resultText: '<script src="/src/">' },
			]
		}, indexFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="/src/|">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="/src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="/src/test.js">' },
			]
		}, aboutFraglUri, workspaceFolderUri);
	});

	test('Empty Path Value', async () => {
		// document: index.fg
		await testCompletion2For('<script src="|">', {
			items: [
				{ label: 'about/', resultText: '<script src="about/">' },
				{ label: 'index.fg', resultText: '<script src="index.fg">' },
				{ label: 'src/', resultText: '<script src="src/">' },
			]
		}, indexFraglUri);
		// document: about.fg
		await testCompletion2For('<script src="|">', {
			items: [
				{ label: 'about.css', resultText: '<script src="about.css">' },
				{ label: 'about.fg', resultText: '<script src="about.fg">' },
				{ label: 'media/', resultText: '<script src="media/">' },
			]
		}, aboutFraglUri);
	});
	test('Incomplete Path', async () => {
		await testCompletion2For('<script src="/src/f|">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="/src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="/src/test.js">' },
			]
		}, aboutFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="../src/f|">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="../src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="../src/test.js">' },
			]
		}, aboutFraglUri, workspaceFolderUri);
	});

	test('No leading dot or slash', async () => {
		// document: index.fg
		await testCompletion2For('<script src="s|">', {
			items: [
				{ label: 'about/', resultText: '<script src="about/">' },
				{ label: 'index.fg', resultText: '<script src="index.fg">' },
				{ label: 'src/', resultText: '<script src="src/">' },
			]
		}, indexFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="src/|">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="src/test.js">' },
			]
		}, indexFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="src/f|">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="src/test.js">' },
			]
		}, indexFraglUri, workspaceFolderUri);

		// document: about.fg
		await testCompletion2For('<script src="s|">', {
			items: [
				{ label: 'about.css', resultText: '<script src="about.css">' },
				{ label: 'about.fg', resultText: '<script src="about.fg">' },
				{ label: 'media/', resultText: '<script src="media/">' },
			]
		}, aboutFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="media/|">', {
			items: [
				{ label: 'icon.pic', resultText: '<script src="media/icon.pic">' }
			]
		}, aboutFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="media/f|">', {
			items: [
				{ label: 'icon.pic', resultText: '<script src="media/icon.pic">' }
			]
		}, aboutFraglUri, workspaceFolderUri);
	});

	test('Trigger completion in middle of path', async () => {
		// document: index.fg
		await testCompletion2For('<script src="src/f|eature.js">', {
			items: [
				{ label: 'feature.js', resultText: '<script src="src/feature.js">' },
				{ label: 'test.js', resultText: '<script src="src/test.js">' },
			]
		}, indexFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="s|rc/feature.js">', {
			items: [
				{ label: 'about/', resultText: '<script src="about/">' },
				{ label: 'index.fg', resultText: '<script src="index.fg">' },
				{ label: 'src/', resultText: '<script src="src/">' },
			]
		}, indexFraglUri, workspaceFolderUri);

		// document: about.fg
		await testCompletion2For('<script src="media/f|eature.js">', {
			items: [
				{ label: 'icon.pic', resultText: '<script src="media/icon.pic">' }
			]
		}, aboutFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="m|edia/feature.js">', {
			items: [
				{ label: 'about.css', resultText: '<script src="about.css">' },
				{ label: 'about.fg', resultText: '<script src="about.fg">' },
				{ label: 'media/', resultText: '<script src="media/">' },
			]
		}, aboutFraglUri, workspaceFolderUri);
	});


	test('Trigger completion in middle of path and with whitespaces', async () => {
		await testCompletion2For('<script src="./| about/about.fg>', {
			items: [
				{ label: 'about/', resultText: '<script src="./about/ about/about.fg>' },
				{ label: 'index.fg', resultText: '<script src="./index.fg about/about.fg>' },
				{ label: 'src/', resultText: '<script src="./src/ about/about.fg>' },
			]
		}, indexFraglUri, workspaceFolderUri);

		await testCompletion2For('<script src="./a|bout /about.fg>', {
			items: [
				{ label: 'about/', resultText: '<script src="./about/ /about.fg>' },
				{ label: 'index.fg', resultText: '<script src="./index.fg /about.fg>' },
				{ label: 'src/', resultText: '<script src="./src/ /about.fg>' },
			]
		}, indexFraglUri, workspaceFolderUri);
	});

	test('Completion should ignore files/folders starting with dot', async () => {
		await testCompletion2For('<script src="./|"', {
			count: 3
		}, indexFraglUri, workspaceFolderUri);
	});

	test('Unquoted Path', async () => {
		/* Unquoted value is not supported in fragl language service yet
		await testCompletion2For(`<div><a href=about/|>`, {
			items: [
				{ label: 'about.fg', resultText: `<div><a href=about/about.fg>` }
			]
		}, testUri);
		*/
	});
});
