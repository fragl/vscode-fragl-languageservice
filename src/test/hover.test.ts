/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import { assertHover, assertHover2 } from './hoverUtil';
import { MarkupContent } from '../fraglLanguageTypes';

suite('FRAGL Hover', () => {
	test('Single', function (): any {
		const descriptionAndReference =
			'The fragl element represents the root of an FRAGL document.' +
			'\n\n' +
			'[MDN Reference](https://developer.mozilla.org/docs/Web/FRAGL/Element/fragl)';

		const fraglContent: MarkupContent = {
			kind: 'markdown',
			value: descriptionAndReference
		};
		const closeHtmlContent: MarkupContent = {
			kind: 'markdown',
			value: descriptionAndReference
		};

		const entityDescription = `Character entity representing '\u00A0', unicode equivalent 'U+00A0'`;


		assertHover('|<fragl></fragl>', void 0, void 0);
		assertHover('<|fragl></fragl>', fraglContent, 1);
		assertHover('<fr|agl></fragl>', fraglContent, 1);
		assertHover('<frag|l></fragl>', fraglContent, 1);
		assertHover('<fragl|></fragl>', fraglContent, 1);
		assertHover('<fragl>|</fragl>', void 0, void 0);
		assertHover('<fragl><|/fragl>', void 0, void 0);
		assertHover('<fragl></|fragl>', closeHtmlContent, 9);
		assertHover('<fragl></f|ragl>', closeHtmlContent, 9);
		assertHover('<fragl></fra|gl>', closeHtmlContent, 9);
		assertHover('<fragl></frag|l>', closeHtmlContent, 9);
		assertHover('<fragl></fragl|>', closeHtmlContent, 9);
		assertHover('<fragl></fragl>|', void 0, void 0);

		assertHover2('<fragl>|&nbsp;</fragl>', '', '');
		assertHover2('<fragl>&|nbsp;</fragl>', entityDescription, 'nbsp;');
		assertHover2('<fragl>&n|bsp;</fragl>', entityDescription, 'nbsp;');
		assertHover2('<fragl>&nb|sp;</fragl>', entityDescription, 'nbsp;');
		assertHover2('<fragl>&nbs|p;</fragl>', entityDescription, 'nbsp;');
		assertHover2('<fragl>&nbsp|;</fragl>', entityDescription, 'nbsp;');
		assertHover2('<fragl>&nbsp;|</fragl>', '', '');

		const noDescription: MarkupContent = {
			kind: 'markdown',
			value: '[MDN Reference](https://developer.mozilla.org/docs/Web/FRAGL/Element/fragl)'
		};
		assertHover2('<fragl|></fragl>', noDescription, 'fragl', undefined, { documentation: false });

		const noReferences: MarkupContent = {
			kind: 'markdown',
			value: 'The fragl element represents the root of an FRAGL document.'
		};
		assertHover2('<fragl|></fragl>', noReferences, 'fragl', undefined, { references: false });
	});
});
