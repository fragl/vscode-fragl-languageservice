/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import * as assert from 'assert';
import * as fraglLanguageService from '../fraglLanguageService';
import { TextDocument } from '../fraglLanguageService';
import 'source-map-support/register';


suite('FRAGL Highlighting', () => {


	function assertHighlights(value: string, expectedMatches: number[], elementName: string | null): void {
		const offset = value.indexOf('|');
		value = value.substr(0, offset) + value.substr(offset + 1);

		const document = TextDocument.create('test://test/test.fg', 'fragl', 0, value);

		const position = document.positionAt(offset);
		const ls = fraglLanguageService.getLanguageService();
		const fraglDoc = ls.parseFRAGLDocument(document);

		const highlights = ls.findDocumentHighlights(document, position, fraglDoc);
		assert.equal(highlights.length, expectedMatches.length);
		for (let i = 0; i < highlights.length; i++) {
			const actualStartOffset = document.offsetAt(highlights[i].range.start);
			assert.equal(actualStartOffset, expectedMatches[i]);
			const actualEndOffset = document.offsetAt(highlights[i].range.end);
			assert.equal(actualEndOffset, expectedMatches[i] + elementName!.length);

			assert.equal(document.getText().substring(actualStartOffset, actualEndOffset).toLowerCase(), elementName);
		}
	}

	test('Single', function (): any {
		assertHighlights('|<fragl></fragl>', [], null);
		assertHighlights('<|fragl></fragl>', [1, 9], 'fragl');
		assertHighlights('<f|ragl></fragl>', [1, 9], 'fragl');
		assertHighlights('<frag|l></fragl>', [1, 9], 'fragl');
		assertHighlights('<fragl|></fragl>', [1, 9], 'fragl');
		assertHighlights('<fragl>|</fragl>', [], null);
		assertHighlights('<fragl><|/fragl>', [], null);
		assertHighlights('<fragl></|fragl>', [1, 9], 'fragl');
		assertHighlights('<fragl></f|ragl>', [1, 9], 'fragl');
		assertHighlights('<fragl></fra|gl>', [1, 9], 'fragl');
		assertHighlights('<fragl></frag|l>', [1, 9], 'fragl');
		assertHighlights('<fragl></fragl|>', [1, 9], 'fragl');
		assertHighlights('<fragl></fragl>|', [], null);
	});

	test('Nested', function (): any {
		assertHighlights('<fragl>|<div></div></fragl>', [], null);
		assertHighlights('<fragl><|div></div></fragl>', [8, 14], 'div');
		assertHighlights('<fragl><div>|</div></fragl>', [], null);
		assertHighlights('<fragl><div></di|v></fragl>', [8, 14], 'div');
		assertHighlights('<fragl><div><div></div></di|v></fragl>', [8, 25], 'div');
		assertHighlights('<fragl><div><div></div|></div></fragl>', [13, 19], 'div');
		assertHighlights('<fragl><div><div|></div></div></fragl>', [13, 19], 'div');
		assertHighlights('<fragl><div><div></div></div></f|ragl>', [1, 31], 'fragl');
		assertHighlights('<fragl><di|v></div><div></div></fragl>', [8, 14], 'div');
		assertHighlights('<fragl><div></div><div></d|iv></fragl>', [19, 25], 'div');
	});

	test('Selfclosed', function (): any {
		assertHighlights('<fragl><|div/></fragl>', [8], 'div');
		assertHighlights('<fragl><|br></fragl>', [8], 'br');
		assertHighlights('<fragl><div><d|iv/></div></fragl>', [13], 'div');
	});

	test('Case insensivity', function (): any {
		assertHighlights('<FRAGL><diV><Div></dIV></dI|v></fragl>', [8, 25], 'div');
		assertHighlights('<FRAGL><diV|><Div></dIV></dIv></fragl>', [8, 25], 'div');
	});

	test('Incomplete', function (): any {
		assertHighlights('<div><ol><li></li></ol></p></|div>', [1, 29], 'div');
	});
});