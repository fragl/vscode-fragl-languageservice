/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import * as assert from 'assert';
import * as fraglLanguageService from '../fraglLanguageService';

import { SymbolInformation, SymbolKind, Location, Range, TextDocument } from '../fraglLanguageService';

suite('FRAGL Symbols', () => {

	const TEST_URI = "test://test/test.fg";

	function asPromise<T>(result: T): Promise<T> {
		return Promise.resolve(result);
	}

	const assertSymbols = function (symbols: SymbolInformation[], expected: SymbolInformation[]) {
		assert.deepEqual(symbols, expected);
	};

	const testSymbolsFor = function (value: string, expected: SymbolInformation[]) {
		const ls = fraglLanguageService.getLanguageService();
		const document = TextDocument.create(TEST_URI, 'fragl', 0, value);
		const fraglDoc = ls.parseFRAGLDocument(document);
		const symbols = ls.findDocumentSymbols(document, fraglDoc);
		assertSymbols(symbols, expected);
	};

	test('Simple', () => {
		testSymbolsFor('<div></div>', [<SymbolInformation>{ containerName: '', name: 'div', kind: <SymbolKind>SymbolKind.Field, location: Location.create(TEST_URI, Range.create(0, 0, 0, 11)) }]);
		testSymbolsFor('<div><input checked id="test" class="checkbox"></div>', [{ containerName: '', name: 'div', kind: <SymbolKind>SymbolKind.Field, location: Location.create(TEST_URI, Range.create(0, 0, 0, 53)) },
		{ containerName: 'div', name: 'input#test.checkbox', kind: <SymbolKind>SymbolKind.Field, location: Location.create(TEST_URI, Range.create(0, 5, 0, 47)) }]);
	});

	test('Id and classes', function () {
		const content = '<fragl id=\'root\'><body id="Foo" class="bar"><div class="a b"></div></body></fragl>';

		const expected = [
			{ name: 'fragl#root', kind: SymbolKind.Field, containerName: '', location: Location.create(TEST_URI, Range.create(0, 0, 0, 82)) },
			{ name: 'body#Foo.bar', kind: SymbolKind.Field, containerName: 'fragl#root', location: Location.create(TEST_URI, Range.create(0, 17, 0, 74)) },
			{ name: 'div.a.b', kind: SymbolKind.Field, containerName: 'body#Foo.bar', location: Location.create(TEST_URI, Range.create(0, 44, 0, 67)) },
		];

		testSymbolsFor(content, expected);
	});

	test('Self closing', function () {
		const content = '<fragl><br id="Foo"><br id=Bar></fragl>';

		const expected = [
			{ name: 'fragl', kind: SymbolKind.Field, containerName: '', location: Location.create(TEST_URI, Range.create(0, 0, 0, 39)) },
			{ name: 'br#Foo', kind: SymbolKind.Field, containerName: 'fragl', location: Location.create(TEST_URI, Range.create(0, 7, 0, 20)) },
			{ name: 'br#Bar', kind: SymbolKind.Field, containerName: 'fragl', location: Location.create(TEST_URI, Range.create(0, 20, 0, 31)) },
		];

		testSymbolsFor(content, expected);
	});

	test('No attrib', function () {
		const content = '<fragl><body><div></div></body></fragl>';

		const expected = [
			{ name: 'fragl', kind: SymbolKind.Field, containerName: '', location: Location.create(TEST_URI, Range.create(0, 0, 0, 39)) },
			{ name: 'body', kind: SymbolKind.Field, containerName: 'fragl', location: Location.create(TEST_URI, Range.create(0, 7, 0, 31)) },
			{ name: 'div', kind: SymbolKind.Field, containerName: 'body', location: Location.create(TEST_URI, Range.create(0, 13, 0, 24)) }
		];

		testSymbolsFor(content, expected);
	});
});
