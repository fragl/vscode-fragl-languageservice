/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import * as assert from 'assert';
import * as fraglLanguageService from '../fraglLanguageService';

import { FraglAttributeValueContext, FraglContentContext, Position, TextDocument } from '../fraglLanguageService';

export interface ExpectedFraglAttributeValue {
  tag: string;
  attribute: string;
  value: string;
  replaceContent: string;
}

export interface ExpectedFraglContent {
}

suite('FRAGL Completion Participant', () => {

  const prepareDocument = (value: string): { document: TextDocument, position: Position } => {
    const offset = value.indexOf('|');
    value = value.substr(0, offset) + value.substr(offset + 1);
    const document = TextDocument.create('test://test/test.fragl', 'fragl', 0, value);
    const position = document.positionAt(offset);
    return { document, position };
  };

  const testFraglAttributeValues = (value: string, expected: ExpectedFraglAttributeValue[]): void => {
    const ls = fraglLanguageService.getLanguageService();
    const { document, position } = prepareDocument(value);

    const actuals: ExpectedFraglAttributeValue[] = [];
    const participant: fraglLanguageService.ICompletionParticipant = {
      onFraglAttributeValue: (context: FraglAttributeValueContext) => {
        const replaceContent = document.getText().substring(document.offsetAt(context.range.start), document.offsetAt(context.range.end));
        assert.equal(context.document, document);
        assert.equal(context.position, position);
        actuals.push({
          tag: context.tag,
          attribute: context.attribute,
          value: context.value,
          replaceContent
        });
      }
    };
    ls.setCompletionParticipants([participant]);
    const fraglDoc = ls.parseFRAGLDocument(document);
    const list = ls.doComplete(document, position, fraglDoc);

    const c = (a1: ExpectedFraglAttributeValue, a2: ExpectedFraglAttributeValue) => {
      return new String(a1.tag + a1.attribute + a1.value).localeCompare(a2.tag + a2.attribute + a2.value);
    };
    assert.deepEqual(actuals.sort(c), expected.sort(c));
  };

  const testFraglContent = (value: string, expected: ExpectedFraglContent[]): void => {
    const ls = fraglLanguageService.getLanguageService();
    const { document, position } = prepareDocument(value);

    const actuals: {}[] = [];
    const participant: fraglLanguageService.ICompletionParticipant = {
      onFraglContent: (context: FraglContentContext) => {
        actuals.push(Object.create(null));
      }
    };
    ls.setCompletionParticipants([participant]);
    const fraglDoc = ls.parseFRAGLDocument(document);
    const list = ls.doComplete(document, position, fraglDoc);
    assert.deepEqual(actuals.length, expected.length);
  };

  test('onFraglAttributeValue', () => {
    testFraglAttributeValues('<|', []);
    testFraglAttributeValues('<div |>', []);
    testFraglAttributeValues('<div class|>', []);
    testFraglAttributeValues('<div>|', []);
    testFraglAttributeValues('<div>|</div>', []);

    testFraglAttributeValues('<div class="|"></div>', [{
      tag: 'div',
      attribute: 'class',
      value: '',
      replaceContent: '""'
    }]);
    testFraglAttributeValues('<div class="|f"></div>', [{
      tag: 'div',
      attribute: 'class',
      value: '',
      replaceContent: '"f"'
    }]);
    testFraglAttributeValues('<div class="foo|"></div>', [{
      tag: 'div',
      attribute: 'class',
      value: 'foo',
      replaceContent: '"foo"'
    }]);
    testFraglAttributeValues(`<div class='foo'|></div>`, [{
      tag: 'div',
      attribute: 'class',
      value: '',
      replaceContent: `'foo'`
    }]);
    testFraglAttributeValues(`<div class=|'foo'></div>`, [{
      tag: 'div',
      attribute: 'class',
      value: '',
      replaceContent: `'foo'`
    }]);
    testFraglAttributeValues(`<div class=|foo></div>`, [{
      tag: 'div',
      attribute: 'class',
      value: '',
      replaceContent: 'foo'
    }]);
    testFraglAttributeValues(`<div class=foo|></div>`, [{
      tag: 'div',
      attribute: 'class',
      value: 'foo',
      replaceContent: 'foo'
    }]);
    testFraglAttributeValues(`<div class=fo|o></div>`, [{
      tag: 'div',
      attribute: 'class',
      value: 'fo',
      replaceContent: 'foo'
    }]);
  });

  test('onFraglContent', () => {
    testFraglContent('<|', []);
    testFraglContent('<div |>', []);
    testFraglContent('<div class|>', []);
    testFraglContent('<div class="|">', []);
    testFraglContent('<div class="foo |">', []);
    testFraglContent('<div class="foo">d|', [{}]);
    testFraglContent('<div class="foo">d|</div>', [{}]);
  });

});