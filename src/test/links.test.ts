
/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import * as assert from 'assert';
import { TextDocument } from '../fraglLanguageTypes';
import * as fraglLanguageService from '../fraglLanguageService';
import { getDocumentContext } from './testUtil/documentContext';

suite('FRAGL Link Detection', () => {

	const ext2lang: { [ext: string]: string } = {
		fragl: 'fragl',
		hbs: 'handlebars'
	};

	function testLinkCreation(modelUrl: string, tokenContent: string, expected: string | null): void {
		const langId = ext2lang[modelUrl.substr(modelUrl.lastIndexOf('.') + 1)] || 'fragl';
		const document = TextDocument.create(modelUrl, langId, 0, `<a href="${tokenContent}">`);
		const ls = fraglLanguageService.getLanguageService();
		const links = ls.findDocumentLinks(document, getDocumentContext());
		assert.equal(links[0] && links[0].target, expected);
	}

	function testLinkDetection(value: string, expectedLinks: { offset: number, length: number, target: string; }[]): void {
		const document = TextDocument.create('file:///test/data/abc/test.fg', 'fragl', 0, value);
		const ls = fraglLanguageService.getLanguageService();
		const links = ls.findDocumentLinks(document, getDocumentContext());
		assert.deepEqual(links.map(l => ({ offset: l.range.start.character, length: l.range.end.character - l.range.start.character, target: l.target })), expectedLinks);
	}

	test('Link creation', () => {
		testLinkCreation('http://model/1.fg', 'javascript:void;', null);
		testLinkCreation('http://model/1.fg', ' \tjavascript:alert(7);', null);
		testLinkCreation('http://model/1.fg', ' #relative', 'http://model/1.fg#relative');
		testLinkCreation('http://model/1.fg', 'file:///C:\\Alex\\src\\path\\to\\file.txt', 'file:///C:\\Alex\\src\\path\\to\\file.txt');
		testLinkCreation('http://model/1.fg', 'http://www.microsoft.com/', 'http://www.microsoft.com/');
		testLinkCreation('http://model/1.fg', 'https://www.microsoft.com/', 'https://www.microsoft.com/');
		testLinkCreation('http://model/1.fg', '//www.microsoft.com/', 'http://www.microsoft.com/');
		testLinkCreation('http://model/x/1.fg', 'a.js', 'http://model/x/a.js');
		testLinkCreation('http://model/x/1.fg', './a2.js', 'http://model/x/a2.js');
		testLinkCreation('http://model/x/1.fg', '/b.js', 'http://model/b.js');
		testLinkCreation('http://model/x/y/1.fg', '../../c.js', 'http://model/c.js');

		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', 'javascript:void;', null);
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', ' \tjavascript:alert(7);', null);
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', ' #relative', 'file:///C:/Alex/src/path/to/file.fg#relative');
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', 'file:///C:\\Alex\\src\\path\\to\\file.txt', 'file:///C:\\Alex\\src\\path\\to\\file.txt');
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', 'http://www.microsoft.com/', 'http://www.microsoft.com/');
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', 'https://www.microsoft.com/', 'https://www.microsoft.com/');
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', '  //www.microsoft.com/', 'http://www.microsoft.com/');
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', 'a.js', 'file:///C:/Alex/src/path/to/a.js');
		testLinkCreation('file:///C:/Alex/src/path/to/file.fg', '/a.js', 'file:///a.js');

		testLinkCreation('https://www.test.com/path/to/file.fg', 'file:///C:\\Alex\\src\\path\\to\\file.txt', 'file:///C:\\Alex\\src\\path\\to\\file.txt');
		testLinkCreation('https://www.test.com/path/to/file.fg', '//www.microsoft.com/', 'https://www.microsoft.com/');
		testLinkCreation('https://www.test.com/path/to/file.fg', '//www.microsoft.com/', 'https://www.microsoft.com/');

		// invalid uris are ignored
		testLinkCreation('https://www.test.com/path/to/file.fg', '%', null);

		// Bug #18314: Ctrl + Click does not open existing file if folder's name starts with 'c' character
		testLinkCreation('file:///c:/Alex/working_dir/18314-link-detection/test.fg', '/class/class.js', 'file:///class/class.js');

		testLinkCreation('http://foo/bar.hbs', '/class/class.js', 'http://foo/class/class.js');
		testLinkCreation('http://foo/bar.hbs', '{{asset foo}}/class/class.js', null);
		testLinkCreation('http://foo/bar.hbs', '{{href-to', null); // issue https://github.com/microsoft/vscode/issues/134334
	});

	test('Link detection', () => {
		testLinkDetection('<img src="foo.png">', [{ offset: 10, length: 7, target: 'file:///test/data/abc/foo.png' }]);
		testLinkDetection('<a href="http://server/foo.fg">', [{ offset: 9, length: 20, target: 'http://server/foo.fg' }]);
		testLinkDetection('<img src="">', []);
		testLinkDetection('<LINK HREF="a.fg">', [{ offset: 12, length: 4, target: 'file:///test/data/abc/a.fg' }]);
		testLinkDetection('<LINK HREF="a.fg\n>\n', []);
		testLinkDetection('<a href=http://www.example.com></a>', [{ offset: 8, length: 22, target: 'http://www.example.com' }]);

		testLinkDetection('<fragl><base href="docs/"><img src="foo.png"></fragl>', [{ offset: 36, length: 7, target: 'file:///test/data/abc/docs/foo.png' }]);
		testLinkDetection('<fragl><base href="http://www.example.com/page.fg"><img src="foo.png"></fragl>', [{ offset: 61, length: 7, target: 'http://www.example.com/foo.png' }]);
		testLinkDetection('<fragl><base href=".."><img src="foo.png"></fragl>', [{ offset: 33, length: 7, target: 'file:///test/data/foo.png' }]);
		testLinkDetection('<fragl><base href="."><img src="foo.png"></fragl>', [{ offset: 32, length: 7, target: 'file:///test/data/abc/foo.png' }]);
		testLinkDetection('<fragl><base href="/docs/"><img src="foo.png"></fragl>', [{ offset: 37, length: 7, target: 'file:///docs/foo.png' }]);

		testLinkDetection('<a href="mailto:<%- mail %>@<%- domain %>" > <% - mail %>@<% - domain %> </a>', []);
	});

	test('Local targets', () => {
		testLinkDetection('<body><h1 id="title"></h1><a href="#title"</a></body>', [{ offset: 35, length: 6, target: 'file:///test/data/abc/test.fg#1,14' }]);
		testLinkDetection('<body><h1 id="title"></h1><a href="file:///test/data/abc/test.fg#title"</a></body>', [{ offset: 35, length: 35, target: 'file:///test/data/abc/test.fg#1,14' }]);
	});

});