# vscode-fragl-languageservice
FRAGL language service forked from [vscode-html-languageservice](https://github.com/microsoft/vscode-html-languageservice).

[![npm Package](https://img.shields.io/npm/v/vscode-fragl-languageservice.svg?style=flat-square)](https://www.npmjs.org/package/vscode-fragl-languageservice)
[![NPM Downloads](https://img.shields.io/npm/dm/vscode-fragl-languageservice.svg)](https://npmjs.org/package/vscode-fragl-languageservice)
[![Azure DevOps Build Status](https://img.shields.io/azure-devops/build/vscode/4c3636fe-3a50-40b9-b8b4-f820ca92886f/22.svg?label=Azure%20DevOps)](https://dev.azure.com/vscode/vscode-fragl-languageservice/_build?definitionId=22)
[![Travis Build Status](https://travis-ci.org/Microsoft/vscode-fragl-languageservice.svg?branch=master)](https://travis-ci.org/Microsoft/vscode-fragl-languageservice)


Why?
----

The _vscode-fragl-languageservice_ contains the language smarts behind the FRAGL editing experience of Visual Studio Code
and the Monaco editor.

 - *doComplete* / *doComplete2* (async) provide completion proposals for a given location.
 - *setCompletionParticipants* allows participant to provide suggestions for specific tokens.
 - *doHover* provides hover information at a given location.
 
 - *format* formats the code at the given range.
 - *findDocumentLinks* finds all links in the document.
 - *findDocumentSymbols* finds all the symbols in the document.
 - *getFoldingRanges* return folding ranges for the given document.
 - *getSelectionRanges* return the selection ranges for the given document.
 ...

 For the complete API see [fraglLanguageService.ts](./src/fraglLanguageService.ts) and [fraglLanguageTypes.ts](./src/fraglLanguageTypes.ts) 

Installation
------------

    npm install --save vscode-fragl-languageservice

Development
-----------

- clone this repo, run yarn
- `yarn test` to compile and run tests


How can I run and debug the service?

- open the folder in VSCode.
- set breakpoints, e.g. in `fraglCompletion.ts`
- run the Unit tests from the run viewlet and wait until a breakpoint is hit:
![image](https://user-images.githubusercontent.com/6461412/94239202-bdad4e80-ff11-11ea-99c3-cb9dbeb1c0b2.png)


How can I run and debug the service inside an instance of VSCode?

- run VSCode out of sources setup as described here: https://github.com/Microsoft/vscode/wiki/How-to-Contribute
- link the fodler of the `vscode-fragl-languageservice` repo to `vscode/extensions/fragl-language-features/server` to run VSCode with the latest changes from that folder:
  - cd `vscode-fragl-languageservice`, `yarn link`
  - cd `vscode/extensions/fragl-language-features/server`, `yarn link vscode-fragl-languageservice`
- run VSCode out of source (`vscode/scripts/code.sh|bat`) and open a `.fg` file
- in VSCode window that is open on the `vscode-fragl-languageservice` sources, run command `Debug: Attach to Node process` and pick the `code-oss` process with the `fragl-language-features` path
![image](https://user-images.githubusercontent.com/6461412/94239296-dfa6d100-ff11-11ea-8e30-6444cf5defb8.png)
- set breakpoints, e.g. in `fraglCompletion.ts`
- in the instance run from sources, invoke code completion in the `.fg` file


License
-------

(MIT License)

Copyright 2021, Jake Russo

With the exceptions of `data/*.json`, which is built upon content from [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web)
and distributed under CC BY-SA 2.5.
